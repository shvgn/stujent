#!/usr/bin/env bash
TARGET=out
SRC_ROOT=src
#MAIN_CLASS_PATH="$SRC_ROOT/javase01/t01/logic/Logic.java"
MAIN_CLASS_PATH="$SRC_ROOT/javase01/t01/main/Main.java"

javac -d $TARGET -cp $SRC_ROOT $MAIN_CLASS_PATH