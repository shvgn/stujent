#!/usr/bin/env bash
TARGET=out
SRC_ROOT=src
TEST_ROOT=test
MAIN_CLASS_PATH="$TEST_ROOT/javase01/t01/main/Test1.java"

javac -d $TARGET -cp $SRC_ROOT:$TEST_ROOT $MAIN_CLASS_PATH
java -cp $TARGET javase01.t01.main.Test1