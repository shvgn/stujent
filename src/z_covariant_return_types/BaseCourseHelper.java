package z_covariant_return_types;

/**
 * Created by shvgn on 06/06/16.
 */
public class BaseCourseHelper extends CourseHelper {

    // In the bytecode, there will be overloaded substitution method
    //
    // public Course getCourse() {
    //     System.out.println("Course");
    //     return new Course();
    // }
    //
    // And it shall be called on CourseHelper variables due to
    // the static binding.

    @Override // FIXME really?
    public BaseCourse getCourse() {
        System.out.println("Course");
        return new BaseCourse();
    }
}
