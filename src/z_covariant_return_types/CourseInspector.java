package z_covariant_return_types;

/**
 * Created by shvgn on 06/06/16.
 */
public class CourseInspector {
    public static void main(String[] args) {
        CourseHelper courseHelper = new BaseCourseHelper();
        Course course = courseHelper.getCourse();

        // Calls statically bound overloaded BaseCourseHelper.getCourse
        // method which returns a Course instance.
        // BaseCourse course1 = courseHelper.getCourse();
        BaseCourse course1 = (BaseCourse) courseHelper.getCourse();

        courseHelper.getCourse();
    }
}
