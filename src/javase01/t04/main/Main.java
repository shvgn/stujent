package javase01.t04.main;

import java.util.Random;

/**
 * Created by shvgn on 27/05/16.
 */
public class Main {

    public static void main(String[] args) {

        //The array must be even-sized!
        int length = 20;

        double[] array = arrayOfRandoms(length);
        double n1, n2;
        double result = 0.0;

        // Show it in calculate the max symmetric pair
        // WONTFIX might have been a separate method
        for (int i = 0; i < array.length; i++) {

            n1 = array[i];
            n2 = array[length - i - 1];

            System.out.printf("%.1f...", n1);
            if (i < length - 1) {
                System.out.print(", ");
            } else {
                System.out.println();
            }

            if (i <= length / 2) {

                if (result < n1 + n2) {
                    result = n1 + n2;
                }
            }

        }

        System.out.println("Result: " + result);
    }

    /**
     * Generate the array of random doubles between 0 and 100 and
     * of the passed length.
     *
     * @param length If the length is odd, it will be incremented.
     */
    private static double[] arrayOfRandoms(int length) {

        if (length % 2 != 0) {
            // Changing the input argument might not be very cool idea
            length++;
        }

        double[] array = new double[length];
        Random rand = new Random();

        // Generating the array
        for (int i = 0; i < array.length; i++) {
            array[i] = rand.nextDouble() * 100;
        }

        return array;
    }
}


/*
Output:
82.2..., 82.6..., 76.4..., 3.5..., 42.5..., 22.6..., 64.5..., 3.8..., 19.4..., 99.8..., 27.0..., 62.5..., 19.9..., 37.0..., 60.2..., 50.8..., 99.3..., 34.8..., 59.2..., 99.8...
Result: 182.00753172143195
 */