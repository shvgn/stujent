package javase01.t06.notepad;

/**
 * Notepad class stores text notes
 */
public class Notepad {

    private final int MIN_CAPACITY = 10;
    private final int MAX_INC_CAPACITY = 100;
    private Note[] notes;   // Notes storage
    private int length;     // Number of notes

    /**
     * Constructor initializes with the capacity equal 10.
     */
    public Notepad() {
        this.notes = new Note[MIN_CAPACITY];
    }

    /**
     * Replace a note at given index with the new one.
     *
     * <p>If there is no note at the index, the note will
     * be added.</p>
     *
     * @return Index of the new note.
     */
    public int edit(int index, Note note) {
        // TODO consider passing text, instead of a Note instance?
        if (index >= length) {
            // No notes here yet, just adding
            return add(note);
        } else {
            notes[index] = note;
            return index;
        }
    }


    /**
     * Adds note to notepad and returns the index of the new note
     */
    public int add(Note note) {
        ensureCapacity(length);
        notes[length] = note;
        length++;
        return length - 1;
    }

    /**
     * Removes note at given index
     */
    public void remove(int index) {
        System.arraycopy(
                notes, index + 1,   // The tail
                notes, index,       // Moves back by one
                length - index);
        length--;
        cutCapacity();
    }

    /**
     * Ensure we have space to add a note at given index
     */
    private void ensureCapacity(int index) {
        if (index >= notes.length) {
            increaseCapacity();
        }
    }

    /**
     * Increases the capacity of the storage by max of 100 entries
     */
    private void increaseCapacity() {
        // FIXME add memory check
        int addCapacity = notes.length / 2;
        addCapacity = addCapacity > MAX_INC_CAPACITY ? MAX_INC_CAPACITY : addCapacity;
        Note[] newNotes = new Note[notes.length + addCapacity];
        System.arraycopy(notes, 0, newNotes, 0, notes.length);
        this.notes = newNotes;
    }

    /**
     * Cut the capacity by half to save memory
     */
    private void cutCapacity() {
        // FIXME add memory check
        int halfCap = notes.length / 2;
        if (halfCap > MIN_CAPACITY && length < halfCap / 2) {
            Note[] newNotes = new Note[halfCap];
            System.arraycopy(notes, 0, newNotes, 0, length);
            this.notes = newNotes;
        }
    }

    /**
     * String representation of a notepad entry
     * <p>
     * <p>Throws IndexOutOfBoundsException when index
     * is greater or equals to the notepad length</p>
     *
     * @param i index of a note
     * @return the text of a note
     */
    public String getNoteText(int i) throws IndexOutOfBoundsException {
        if (i >= length) {
            throw new IndexOutOfBoundsException("Index (" + i + ") >= length (" + length + ")");
        }
        StringBuilder sb = new StringBuilder(3);
        sb = appendStringBuilderFormat(sb, i);
        return sb.toString();
    }

    /**
     * String representation of notepad
     *
     * @return Note text
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder(length * 6);
        for (int i = 0; i < length; i++) {
            sb = appendStringBuilderFormat(sb, i).append("\n");
        }
        return sb.toString();
    }

    /**
     * Generates the chain of StringBuilder.append calls for a given note
     *
     * @param sb StringBuilder instance
     * @param i  note index
     * @return StringBuilder with append parts of the note
     */
    private StringBuilder appendStringBuilderFormat(StringBuilder sb, int i) {
        return sb.append("Note #").append(i).append(":\n")
                .append(notes[i].toString()).append("\n");
    }

    /**
     * Number of notes in the notepad
     */
    public int getLength() {
        return length;
    }
}
