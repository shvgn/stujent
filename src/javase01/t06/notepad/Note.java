package javase01.t06.notepad;

/**
 * Note class contains an arbitrary text record
 */
public class Note {
    private String text;

    public Note(String text) {
        this.text = text;
    }

    /**
     * Get the text of the note
     */
    public String getText() {
        return text;
    }

    /**
     * Set the text of the note
     */
    public void setText(String text) {
        this.text = text;
    }

    /**
     * Returns the text of the note
     */
    @Override
    public String toString() {
        return text;
    }
}
