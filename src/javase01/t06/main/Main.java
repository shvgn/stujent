package javase01.t06.main;

import javase01.t06.notepad.Note;
import javase01.t06.notepad.Notepad;

/**
 * Created by shvgn on 27/05/16.
 */
public class Main {

    public static void main(String[] args) {

        Notepad np = new Notepad();

        np.add(new Note("Lorem ipsum"));
        np.add(new Note("Wow what is it?"));
        np.add(new Note("Baby don't hurt me, don't hurt me, no more..."));
        np.add(new Note("Hundred Suns is f*cking awesome!"));


        System.out.println(np);


        System.out.println("----------------");


        np.remove(2);
        np.edit(0, new Note("EDITED Lorem Ipsum Dolor Sit Amet"));

        System.out.println(np);

    }


}
