package javase01.t03.main;

/**
 * Created by shvgn on 27/05/16.
 */
public class Main {
    public static void main(String[] args) {

        double from = 0;
        double to = 4;
        double step = 0.05 * Math.PI;

        try {

            showTable(from, to, step);

        } catch (Exception e) {
            System.out.println("Invalid parameters: " + e.getMessage());
            System.exit(1);
        }
    }

    /**
     * The function
     */
    private static double function(double x) {
        return Math.tan(2 * x) - 3;
    }

    /**
     * Print results
     */
    public static void showTable(double from, double to, double step) throws Exception {
        if (from >= to) {
            throw new Exception("Expected from < to, got from = " + from + ", to = " + to);
        }

        if (step <= 0) {
            throw new Exception("Expected step > 0, got " + step);
        }

        double x = from;
        for (; x <= to; x += step) {
            System.out.printf("%.4f  %.4f\n", x, function(x));
        }
    }

}
