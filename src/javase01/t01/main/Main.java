package javase01.t01.main;

/**
 * Created by shvgn on 26/05/16.
 */

import javase01.t01.logic.Logic;

public class Main {

    public static void main(String[] args) {
        Logic logic = new Logic();
        System.out.println(logic.method());
    }
}
