package javase01.t02.main;

import javase01.t02.sequence.SimpleSequence;

/**
 * Created by shvgn on 27/05/16.
 */
public class Main {
    public static void main(String[] args) {
        SimpleSequence seq = new SimpleSequence();
        try {
            seq.setEps(0.0023);
        } catch (Exception e) {
            System.out.println("Invalid parameter passed: " + e.getMessage());
            System.exit(1);
        }

        seq.show();

    }
}

/*
   for eps = 0.0023

   1: 0.250000
   2: 0.111111
   3: 0.062500
   4: 0.040000
   5: 0.027778
   6: 0.020408
   7: 0.015625
   8: 0.012346
   9: 0.010000
  10: 0.008264
  11: 0.006944
  12: 0.005917
  13: 0.005102
  14: 0.004444
  15: 0.003906
  16: 0.003460
  17: 0.003086
  18: 0.002770
  19: 0.002500
  20: 0.002268 < 0.002300 -- the end.
 */