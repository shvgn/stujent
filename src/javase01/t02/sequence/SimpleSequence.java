package javase01.t02.sequence;

/**
 * Created by shvgn on 27/05/16.
 */
public class SimpleSequence {

    private double eps;

    /**
     * Shows the sequence until it meets the member which is less than eps
     */
    public void show() {
        int i = 1; // counter
        double value = member(i); // sequence member

        // Do it at least once
        do {
            System.out.printf("%4d: %.6f\n", i, value);
            i++;
            value = member(i);
        } while (value >= eps);

        System.out.printf("%4d: %.6f < %.6f -- the end.\n", i, value, eps);
    }

    /**
     * Calculates members of the sequence
     *
     * @param n index of the member
     * @return value of the member
     */
    private double member(int n) {
        return 1.0 / (1.0 + n) / (1.0 + n);
    }

    /**
     * Returns eps
     */
    public double getEps() {
        return eps;
    }

    /**
     * Sets eps
     *
     * <p>Throws Exception if it doesn't satisfy 0 &lt; eps &lt;= 1
     *
     * @param eps the bounding value
     * @throws Exception
     */
    public void setEps(double eps) throws Exception {

        if (eps <= 0 || eps > 1) {
            throw new Exception("SimpleSequence requires 0 < eps <= 1, got " + eps);
        }

        this.eps = eps;
    }

}
