package javase02.t04;

import javase02.t02.Stationery;

import java.util.Comparator;

/**
 * Created by shvgn on 11/06/16.
 */
public class CompareStationeryByNameAndPrice implements Comparator<Stationery> {
    @Override
    public int compare(Stationery s1, Stationery s2) {
        Comparator<Stationery> byName = new CompareStationeryByName();
        int res = byName.compare(s1, s2);
        if (res != 0) {
            return res;
        }

        Comparator<Stationery> byPrice = new CompareStationeryByPrice();
        return byPrice.compare(s1, s2);
    }
}
