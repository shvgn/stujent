package javase02.t04;

import javase02.t02.Stationery;

import java.util.Comparator;

/**
 * Используйте “набор новичка”, созданный в задании 3, (или любую
 * другую коллекцию объектов); отсортируйте вещи в этом наборе по
 * стоимости, по наименованию, по стоимости и наименованию.
 */
public class CompareStationeryByPrice implements Comparator<Stationery> {
    @Override
    public int compare(Stationery s1, Stationery s2) {
        return Double.compare(s1.getPrice(), s2.getPrice());
    }
}
