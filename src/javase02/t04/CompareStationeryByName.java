package javase02.t04;

import javase02.t02.Stationery;

import java.util.Comparator;

/**
 * Created by shvgn on 11/06/16.
 */
public class CompareStationeryByName implements Comparator<Stationery> {
    @Override
    public int compare(Stationery s1, Stationery s2) {
        return String.CASE_INSENSITIVE_ORDER.compare(s1.getVendor(), s2.getVendor());
    }
}
