package javase02.t02;

/**
 * Created by shvgn on 11/06/16.
 */
public class Pen extends Stationery {
    public Pen(double price) {
        super(price);
    }

    public Pen(double price, String vendor) {
        super(price, vendor);
    }
}
