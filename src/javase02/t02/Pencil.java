package javase02.t02;

/**
 * Created by shvgn on 11/06/16.
 */
public class Pencil extends Stationery {
    public Pencil(double price) {
        super(price);
    }

    public Pencil(double price, String vendor) {
        super(price, vendor);
    }
}
