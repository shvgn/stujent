package javase02.t02;

/**
 * Напишите приложение, позволяющее вести учет канцелярских
 * товаров на рабочем месте сотрудника. Определите полную
 * стоимость канцтоваров у определенного сотрудника.
 */
abstract public class Stationery {
    private double price;
    private String vendor = "";

    /**
     * Constructor with one mandatory field
     *
     * @param price the price of the thing
     */
    public Stationery(double price) {
        this.price = price;
    }

    /**
     * Constructor for price and vendor
     *
     * @param price  price of the thing
     * @param vendor vendor name of the thing
     */
    public Stationery(double price, String vendor) {
        this(price);
        this.vendor = vendor;
    }

    /**
     * @return vendoe name of the thing
     */
    public String getVendor() {
        return vendor;
    }

    /**
     * @return price of this thing
     */
    public double getPrice() {
        return price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Stationery that = (Stationery) o;

        return Double.compare(that.price, price) == 0;

    }

    @Override
    public int hashCode() {
        long temp = Double.doubleToLongBits(price);
        return (int) (temp ^ (temp >>> 32));
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.getClass().getSimpleName())
                .append(": {")
                .append("price: ").append(price)
                .append(", ")
                .append("vendor: ").append(vendor)
                .append("}");
        return sb.toString();
    }
}
