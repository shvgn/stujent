package javase02.t02;

import javase02.t04.CompareStationeryByName;
import javase02.t04.CompareStationeryByPrice;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.function.Consumer;

public class EmployeeDesktop {
    private List<Stationery> stationary;

    public EmployeeDesktop() {
        stationary = new ArrayList<>();
    }

    public void addStationery(Stationery item) {
        stationary.add(item);
    }

    /**
     * Returns the total price of stationary items
     * on a desktop
     */
    public double totalStationaryPrice() {
        double price = 0.0;
        for (Stationery st : stationary) {
            price += st.getPrice();
        }
        return price;
    }

    /**
     * @return List of stationery items
     */
    public List<Stationery> getList() {
        List<Stationery> copy = new ArrayList<>();
        // can be replaced with lambda
        stationary.forEach(new Consumer<Stationery>() {
            @Override
            public void accept(Stationery stationery) {
                copy.add(stationery);
            }
        });
        return copy;
    }

    /**
     * @param comparator
     * @return sorted List of stationery items
     */
    public List<Stationery> getList(Comparator<? super Stationery> comparator) {
        List<Stationery> copy = getList();
        copy.sort(comparator);
        return copy;
    }

    public void sort(Comparator<? super Stationery> comparator) {
        stationary.sort(comparator);
    }

    public List<Stationery> sortByPrice() {
        sort(new CompareStationeryByPrice());
        return getList();
    }

    public List<Stationery> sortByName() {
        sort(new CompareStationeryByName());
        return getList();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("EmployeeDesktop: {\n");

        Iterator<Stationery> it = stationary.iterator();
        while (it.hasNext()) {
            sb.append("\t").append(it.next().toString()).append("\n");
        }
        sb.append("}");
        return sb.toString();
    }
}
