package javase02.t03;

import javase02.t02.EmployeeDesktop;
import javase02.t02.Pen;
import javase02.t02.Pencil;
import javase02.t02.Stationery;

import java.util.List;

public class NoviceStationarySet extends EmployeeDesktop {
    private List<Stationery> list;

    public NoviceStationarySet() {
        // If a constructor does not explicitly invoke a superclass
        // constructor, the Java compiler automatically inserts a call
        // to the no-argument constructor of the superclass. I
        // super();
        this.addStationery(new Pen(0.5));
        this.addStationery(new Pen(0.5));
        this.addStationery(new Pencil(0.5));
        this.addStationery(new Pencil(0.5));
        this.addStationery(new Notepad(1.0));
    }
}
