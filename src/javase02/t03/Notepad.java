package javase02.t03;

import javase02.t02.Stationery;

/**
 * Разработайте иерархию канцелярских товаров.
 * Создайте “набор новичка”, используя созданную иерархию.
 */
public class Notepad extends Stationery {
    public Notepad(double price) {
        super(price);
    }

    public Notepad(double price, String vendor) {
        super(price, vendor);
    }
}
