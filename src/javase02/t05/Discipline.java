package javase02.t05;


import java.util.HashMap;

/**
 * Discipline works as a group of students
 * <p>Каждая дисциплина в отдельности определяет, целыми или вещественными
 * будут оценки по ней.  Для огранизации перечня дисциплин можно использовать
 * перечисление.
 */
public enum Discipline {
    MECHANICS(false),
    THERMODYNAMICS(false),
    QUANTUM_MECHANICS(true),
    OPTICS(true);

    public final Score<? extends Number> scores;

    /**
     * Constructor for the score type
     *
     * @param scoresAreDouble If <code>true</code> points are Double,
     *                        otherwise points are of type Integer.
     */
    Discipline(boolean scoresAreDouble) {
        if (scoresAreDouble) {
            scores = new Score<Double>();
        } else {
            scores = new Score<Integer>();
        }
    }

    public void enroll(Student student) {
        scores.enroll(student);
    }


    /**
     * Inner class to control the scores logic
     *
     * @param <T> Double or Integer
     */
    public class Score<T extends Number> {
        private HashMap<Student, T> scores;

        public Score() {
            scores = new HashMap<>();
        }

        @SuppressWarnings("unchecked")
        public void enroll(Student student) {
            scores.put(student, (T) new Integer(0));
        }

        @SuppressWarnings("unchecked")
        public void addScore(Student student, T points) {
            T current = scores.get(student);
            T future = (T) new Double(current.doubleValue() + points.doubleValue());
            scores.put(student, future);
        }

        /**
         * Get the score of a student
         *
         * @param student
         * @return null or the score boxed in Integer or Double
         */
        public T getScores(Student student) {
            if (scores.containsKey(student)) {
                return scores.get(student);
            }
            return null;
        }
    }

}
