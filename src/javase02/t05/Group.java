package javase02.t05;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Разработайте приложение, позволяющее формировать группы студентов
 * по разным дисциплинам. Состав групп может быть разным. Каждая
 * дисциплина в отдельности определяет, целыми или вещественными
 * будут оценки по ней. Необходимо найти группы, в которые входит
 * студент X, и сравнить его оценки. Для огранизации перечня дисциплин
 * можно использовать перечисление.
 */
public class Group {
    public final Discipline discipline;
    private HashMap<Student, Number> scores;


    Group(Discipline discipline) {
        this.discipline = discipline;
        this.scores = new HashMap<>();
    }

    /**
     * Get student list enrolled in class
     */
    public List<Student> getStudentsList() {
        List<Student> list = new ArrayList<>();
        list.addAll(scores.keySet());
        return list;
    }

    /**
     * Add student to group
     */
    public void enroll(Student student) {
        if (!scores.containsKey(student)) {
            scores.put(student, 0);
        }
    }

    /**
     * Return the copy of students hashmap with their scores
     *
     * @return
     */
    @SuppressWarnings("unchecked")
    public HashMap<Student, Number> getScores() {
        return (HashMap<Student, Number>) scores.clone();
    }


}

