package javase02.t05;


import java.util.HashMap;

public class Student {
    private String name;

    public Student(String name) {
        setName(name);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String toString() {
        return "Student (" + name + ")";
    }

    /**
     * Return total stats of a student
     */
    public HashMap<Discipline, Number> getScores() {
        HashMap<Discipline, Number> results = new HashMap<>();
        for (Discipline d : Discipline.values()) {
            Number res = d.scores.getScores(this);
            if (res != null) {
                results.put(d, res);
            }
        }
        return results;
    }
}
