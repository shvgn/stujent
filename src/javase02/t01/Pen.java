package javase02.t01;

/**
 * Разработайте спецификацию и создайте класс Ручка (Pen).
 * Определите в этом классе методы equals(), hashCode() и toString().
 */
public class Pen {
    private final String color;
    private double price;

    public Pen(String color, double price) {
        this.color = color;
        this.price = price;
    }

    @Override
    public String toString() {
        return "Pen {color: " + color + ", price: " + price + "}";
    }

    @Override
    public int hashCode() {
        return this.color.hashCode() + (int) this.price;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return false;
        }
        if (obj.getClass() != this.getClass()) {
            return false;
        }
        Pen pen2 = (Pen) obj;
        return pen2.color.equals(this.color);
    }

    public String getColor() {
        return color;
    }

    public double getPrice() {
        return price;
    }
}
