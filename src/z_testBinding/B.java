package z_testBinding;


public class B extends A {
    public final static String className = "B";

    public String getClassNameString() {
        return B.className;
    }
}
