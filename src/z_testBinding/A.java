package z_testBinding;


public class A {
    public final static String className = "A";

    public String getClassNameString() {
        return A.className;
    }
}
