package z_testBinding;

/**
 *
 * @see [http://stackoverflow.com/a/22006091/6033985]
 * Here are few important difference between static and dynamic binding
 * <p>1) Static binding in Java occurs during Compile
 * time while Dynamic binding occurs during Runtime.
 * <p>2) private, final and static methods and variables
 * uses static binding and bonded by compiler while
 * virtual methods are bonded during runtime based
 * upon runtime object.
 * <p>3) Static binding uses Type(Class in Java) information
 * for binding while Dynamic binding uses Object to
 * resolve binding.
 * <p>4) Overloaded methods are bonded using static binding
 * while overridden methods are bonded using dynamic
 * binding at runtime.
 */
public class Z {
    /**
     * Returns the AX, where X is the class of the argument
     */
    public String m(A a) {
        return "A" + a.getClassNameString();
    }

    /**
     * Returns the BX, where X is the class of the argument
     */
    public String m(B b) {
        return "B" + b.getClassNameString();
    }

    /**
     * Returns the CX, where X is the class of the argument
     */
    public String m(C c) {
        return "C" + c.getClassNameString();
    }

    /**
     * Returns the DX, where X is the class of the argument
     */
    public String m(D d) {
        return "D" + d.getClassNameString();
    }

}
