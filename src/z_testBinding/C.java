package z_testBinding;


public class C extends B {
    public final static String className = "C";

    public String getClassNameString() {
        return C.className;
    }
}
