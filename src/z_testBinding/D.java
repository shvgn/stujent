package z_testBinding;


public class D extends A {
    public final static String className = "D";

    public String getClassNameString() {
        return D.className;
    }
}
