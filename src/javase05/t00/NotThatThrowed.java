package javase05.t00;

import java.rmi.RemoteException;

/**
 * Created by shvgn on 16/06/16.
 */
public class NotThatThrowed {
    public static void main(String[] args) {
        NotThatThrowed ntt = new NotThatThrowed();

        try {
            ntt.doit();
        } catch (Exception e) {
            System.out.println("Common exception");
        }
    }

    /**
     * No reaction to incorrect <code>throw</code> declaration
     *
     * @throws RemoteException
     */
    private void doit() throws RemoteException {
        try {
            // throw new RemoteException("Ha-ha!");
            throw new SecurityException("Ha-ha!");
        } catch (Exception e) {
            System.out.println("La-la!");
            throw e;
        }
    }
}
