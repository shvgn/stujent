package javase05.t00;

import java.io.FileInputStream;


public class TryWithRes {
    public static void main(String[] args) {
        String path = "/Users/shvgn/dev/java/test.txt";

        try (FileInputStream in = new FileInputStream(path)) {

            int bytesRead = in.read();
            System.out.println("Bytes read: " + bytesRead);
            System.out.println(in.toString());

        } catch (Exception e) {

            // System.out.println(in);        // Unavailable
            System.err.println(e.getMessage());

        } finally {

            System.out.println("Finally");

        }
    }
}

