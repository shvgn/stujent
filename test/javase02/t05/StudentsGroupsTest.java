package javase02.t05;

import org.junit.Test;

/*
 * Разработайте приложение, позволяющее формировать группы студентов
 * по разным дисциплинам. Состав групп может быть разным. Каждая
 * дисциплина в отдельности определяет, целыми или вещественными
 * будут оценки по ней. Необходимо найти группы, в которые входит
 * студент X, и сравнить его оценки. Для огранизации перечня дисциплин
 * можно использовать перечисление.
*/
public class StudentsGroupsTest {

    private Student[] students = new Student[]{
            new Student("Nick Core"),
            new Student("Bill Hendricks"),
            new Student("Lora Palman"),
            new Student("Ginger Black"),
            new Student("Chu Wan Hoe"),
            new Student("Boris Ivanov"),
            new Student("Concita Gerreira")
    };

    private Group[] groups;


    @Test
    public void showDisciplines() {
        for (Discipline d : Discipline.values()) {
            System.out.println(d);
        }
    }

    @Test
    public void setGroups() {
        groups = new Group[]{
                new Group(Discipline.MECHANICS),
                new Group(Discipline.THERMODYNAMICS),
                new Group(Discipline.QUANTUM_MECHANICS),
                new Group(Discipline.OPTICS)
        };

        groups[0].enroll(students[0]);
        groups[0].enroll(students[1]);
        groups[0].enroll(students[2]);
        groups[0].enroll(students[5]);
        groups[0].enroll(students[6]);
//        groups[0].discipline.scores.addScore(students[0], 3);
//        groups[0].discipline.scores.addScore(students[1], 3);
//        groups[0].discipline.scores.addScore(students[2], 3);
//        groups[0].discipline.scores.addScore(students[5], 3);
//        groups[0].discipline.scores.addScore(students[6], 3);

        groups[1].enroll(students[1]);
        groups[1].enroll(students[2]);
        groups[1].enroll(students[3]);
        groups[1].enroll(students[5]);
        groups[1].enroll(students[6]);

        groups[2].enroll(students[1]);
        groups[2].enroll(students[2]);
        groups[2].enroll(students[3]);
        groups[2].enroll(students[5]);

        groups[3].enroll(students[1]);
        groups[3].enroll(students[2]);
        groups[3].enroll(students[4]);
        groups[3].enroll(students[6]);

        for (Group g : groups) {
            System.out.println(g.getScores());
        }

        // for (Student st : students) {
        // HashMap<Discipline, Number> scores = Discipline.getStudentStats(st);
        // System.out.println(scores);
        // }
    }

}