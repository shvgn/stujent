package javase02.t04;

import javase02.t02.EmployeeDesktop;
import javase02.t02.Pen;
import javase02.t02.Pencil;
import javase02.t02.Stationery;
import javase02.t03.Notepad;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by shvgn on 11/06/16.
 */
public class CompareStationeryTest {
    private double[] prices = {1.0, 1.5, 1.5, 3.5, 5.0};
    private String[] vendors = {"Corbina", "HM", "Karandash", "Karandash", "Unknown Vendor"};

    private EmployeeDesktop getTheDesktopSet() {
        EmployeeDesktop ed = new EmployeeDesktop();
        Stationery[] items = {
                new Pen(prices[0], vendors[4]),
                new Pencil(prices[1], vendors[3]),
                new Pen(prices[2], vendors[0]),
                new Notepad(prices[3], vendors[2]),
                new Pen(prices[4], vendors[1])
        };

        for (Stationery item : items) {
            ed.addStationery(item);
        }

        return ed;
    }

    @Test
    public void byName() throws Exception {
        EmployeeDesktop ed = getTheDesktopSet();
        ed.sortByName();
        System.out.println(ed);
        List<Stationery> edlist = ed.getList();
        int len = prices.length < vendors.length ? prices.length : vendors.length;
        for (int i = 0; i < len; i++) {
            assertEquals(edlist.get(i).getVendor(), vendors[i]);
        }
    }

    @Test
    public void byPrice() throws Exception {
        EmployeeDesktop ed = getTheDesktopSet();
        ed.sortByPrice();
        System.out.println(ed);
        List<Stationery> edlist = ed.getList();
        int len = prices.length < vendors.length ? prices.length : vendors.length;
        for (int i = 0; i < len; i++) {
            assertEquals(edlist.get(i).getPrice(), prices[i], 0.001);
        }
    }

    @Test
    public void byNameAndPrice() throws Exception {
        EmployeeDesktop ed = getTheDesktopSet();
        ed.sort(new CompareStationeryByNameAndPrice());
        System.out.println(ed);

        List<Stationery> edlist = ed.getList();
        int len = prices.length < vendors.length ? prices.length : vendors.length;

        for (int i = 0; i < len; i++) {
            assertEquals(edlist.get(i).getVendor(), vendors[i]);
            if (i > 0 && edlist.get(i - 1).getVendor().equals(edlist.get(i).getVendor())) {
                assertTrue(edlist.get(i - 1).getPrice() <= edlist.get(i).getPrice());
            }
        }

    }

}