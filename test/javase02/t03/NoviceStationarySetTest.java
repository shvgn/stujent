package javase02.t03;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by shvgn on 11/06/16.
 */
public class NoviceStationarySetTest {
    @Test
    public void shouldCost3() throws Exception {
        NoviceStationarySet s = new NoviceStationarySet();
        assertEquals(3.0, s.totalStationaryPrice(), 0.001);
    }

}