package javase02.t02;

import org.junit.Test;

import static org.junit.Assert.assertEquals;


public class EmployeeDesktopTest {
    @Test
    public void addStationery() throws Exception {
        Pen p1 = new Pen(1.0);
        Pencil p2 = new Pencil(1.0);
        EmployeeDesktop ed = new EmployeeDesktop();
        ed.addStationery(p1);
        ed.addStationery(p2);
    }

    @Test
    public void totalStationaryPrice() throws Exception {
        double p1 = 1.5;
        double p2 = 5.5;
        double p3 = 2.0;
        double p4 = 1.5;

        Pen pen1 = new Pen(p1);
        Pen pen2 = new Pen(p2);
        Pencil pencil1 = new Pencil(p3);
        Pencil pencil2 = new Pencil(p4);

        EmployeeDesktop ed = new EmployeeDesktop();
        ed.addStationery(pen1);
        ed.addStationery(pen2);
        ed.addStationery(pencil1);
        ed.addStationery(pencil2);

        double total = p1 + p2 + p3 + p4;
        assertEquals(total, ed.totalStationaryPrice(), 0.001);
    }

}