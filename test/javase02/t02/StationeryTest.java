package javase02.t02;

import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by shvgn on 11/06/16.
 */
public class StationeryTest {
    @Test
    public void equalShouldBeEqual() {

        // Equality
        double price = 3.1;

        Pen p1 = new Pen(price);
        Pen p2 = new Pen(price);
        Pencil p3 = new Pencil(price);
        Pencil p4 = new Pencil(price);

        assertTrue(p1.equals(p2));
        assertTrue(p2.equals(p1));
        assertTrue(p3.equals(p4));
        assertTrue(p4.equals(p3));

        // Inequality
        assertFalse(p1.equals(null));
        assertFalse(p3.equals(null));

        double otherPrice = 1.6;

        Pen op1 = new Pen(otherPrice);
        assertFalse(p1.equals(op1));
        assertFalse(op1.equals(p1));

        Pencil op3 = new Pencil(otherPrice);
        assertFalse(p3.equals(op3));
        assertFalse(op3.equals(p3));
    }


}
