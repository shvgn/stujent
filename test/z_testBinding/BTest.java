package z_testBinding;

import org.junit.Test;

import static org.junit.Assert.assertEquals;


public class BTest {
    private B b = new B();

    @Test
    public void classNameToString() throws Exception {
        assertEquals("B", b.getClassNameString());
    }

}