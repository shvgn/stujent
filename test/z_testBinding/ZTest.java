package z_testBinding;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ZTest {


    private Z z = new Z();

    @Test
    public void staticBindingForSuperclassType() throws Exception {
        A aa = new A();
        A ab = new B();
        A ac = new C();
        A ad = new D();

        // Early (static) binding because all
        // these variables are of type A and are
        // used in overloaded methods
        assertEquals("AA", z.m(aa));
        assertEquals("AB", z.m(ab));
        assertEquals("AC", z.m(ac));
        assertEquals("AD", z.m(ad));
    }

    @Test
    public void staticBindingForCorrespondingTypes() throws Exception {
        A a = new A();
        B b = new B();
        C c = new C();
        D d = new D();

        // Early (static) binding, but all
        // these variables are of their
        // corresponding types
        assertEquals("AA", z.m(a));
        assertEquals("BB", z.m(b));
        assertEquals("CC", z.m(c));
        assertEquals("DD", z.m(d));

    }

    @Test
    public void dynamicBindingWithoutVariableTypes() throws Exception {
        // Late (dynamic) binding, because these
        // types are taken right from objects at
        // runtime
        assertEquals("AA", z.m(new A()));
        assertEquals("BB", z.m(new B()));
        assertEquals("CC", z.m(new C()));
        assertEquals("DD", z.m(new D()));
    }

}