package z_testBinding;

import org.junit.Test;

import static org.junit.Assert.assertEquals;


public class ATest {
    private A a = new A();

    @Test
    public void classNameToString() throws Exception {
        assertEquals("A", a.getClassNameString());
    }

}