package z_testBinding;

import org.junit.Test;

import static org.junit.Assert.assertEquals;


public class CTest {
    private C c = new C();

    @Test
    public void classNameToString() throws Exception {
        assertEquals("C", c.getClassNameString());
    }


}