package z_testBinding;

import org.junit.Test;

import static org.junit.Assert.assertEquals;


public class DTest {
    private D d = new D();

    @Test
    public void classNameToString() throws Exception {
        assertEquals("D", d.getClassNameString());
    }


}