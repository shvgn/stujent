package javase01.t01.main;

import javase01.t01.logic.Logic;

/**
 * Created by shvgn on 26/05/16.
 */
public class Test1 {

    public static void main(String[] args) {
        Logic logic = new Logic();
        System.out.println(logic.method().equals("I am a string in Logic."));
    }
}
